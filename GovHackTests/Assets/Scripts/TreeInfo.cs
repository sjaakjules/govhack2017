﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeInfo  {
    public Vector3 position;

    public string[] treeInfo;

    public TreeInfo(string[] CSVData)
    {
        position = GPSManager.getUnityPosition(double.Parse(CSVData[1]), double.Parse(CSVData[0]));

        treeInfo = (string[])CSVData.Clone();

    }

}

public static class CustomTreeExtensions
{
    public static Vector3[] getPositions(this TreeInfo[] tree, int[] indicies)
    {
        Vector3[] listOut = new Vector3[indicies.Length];
        for (int i = 0; i < indicies.Length; i++)
        {
            listOut[i] = tree[indicies[i]].position;
        }
        return listOut;
    }


    public static Vector3[] getAllPositions(this TreeInfo[] tree)
    {
        Vector3[] listOut = new Vector3[tree.Length];
        for (int i = 0; i < tree.Length; i++)
        {
            listOut[i] = tree[i].position;
        }
        return listOut;
    }
}
