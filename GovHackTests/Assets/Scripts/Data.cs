﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Data : MonoBehaviour
{

    public static HexGrid grid;


    public static TreeInfo[] treeList;

    public static Vector3 playerPosition;

    public static int currentHexIndex;
    public static List<int> surroundingHexIndex = new List<int>();
    public static List<TreeInfo> currentTrees = new List<TreeInfo>();
    public static List<TreeInfo> surroundingTrees = new List<TreeInfo>();
    



    // Use this for initialization
    IEnumerator Start()
    {

        while (!CSVManager.hasDigested)
        {
            yield return new WaitForSeconds(1);
        }

           playerPosition = GPSManager.getAveragePostion;

        findHexIndexSlow();
        findNeighbours();
        InvokeRepeating("findHexCell", 1, 0.01f);

    }

    // Update is called once per frame
    void Update()
    {
        playerPosition = GPSManager.getAveragePostion;
      //  playerPosition = transform.position;
        
    }

    void findHexIndexSlow()
    {
        playerPosition = transform.position;

        float minDist = float.MaxValue;
        int minIndex = -1;

        for (int i = 0; i < grid.cells.Count; i++)
        {
            if (Vector3.Distance(playerPosition, grid.cells[i].CellPosition) < minDist)
            {
                minDist = Vector3.Distance(playerPosition, grid.cells[i].CellPosition);
                minIndex = i;
            }
        }
        if (minIndex >= 0)
        {
            currentHexIndex = minIndex;
        }
    }

    void findNeighbours()
    {
        float errorDistance = 5;
        float minDist = float.MaxValue;
        float tempDist;
        List<int> neighbourIndex = new List<int>();
        for (int i = 0; i < grid.cells.Count; i++)
        {
            tempDist = Vector3.Distance(grid.cells[currentHexIndex].CellPosition, grid.cells[i].CellPosition);
            if (i != currentHexIndex && tempDist < minDist - errorDistance)
            {
                neighbourIndex.Clear();
                neighbourIndex.Add(i);
                minDist = tempDist;
            }
            else if (i != currentHexIndex &&  tempDist <= minDist + errorDistance)
            {
                neighbourIndex.Add(i);
            }
        }
        surroundingHexIndex = new List<int>(neighbourIndex);

        surroundingTrees.Clear();
        for (int i = 0; i < surroundingHexIndex.Count; i++)
        {
            for (int j = 0; j < grid.cells[surroundingHexIndex[i]].treeIndecies.Count; j++)
            {
                surroundingTrees.Add(treeList[grid.cells[surroundingHexIndex[i]].treeIndecies[j]]);
            }
        }

    }

    /// <summary>
    /// This loops each of hte surrounding hex cells, if one is closer than the current hex cell then it will update that hex cell and find new neighbours
    /// </summary>
    void findHexCell()
    {
        float minDist = Vector3.Distance(playerPosition, grid.cells[currentHexIndex].CellPosition);
        float tempDist;
        bool updatePosition = false;
        

        if (surroundingHexIndex.Count < 4)
        {
            findNeighbours();
        }
        for (int i = 0; i < surroundingHexIndex.Count; i++)
        {
            tempDist = Vector3.Distance(playerPosition, grid.cells[surroundingHexIndex[i]].CellPosition);
            if (tempDist < minDist)
            {
                currentHexIndex = surroundingHexIndex[i];
                updatePosition = true;
            }
        }
        if (updatePosition)
        {
            currentTrees.Clear();
            for (int i = 0; i < grid.cells[currentHexIndex].treeIndecies.Count; i++)
            {
                currentTrees.Add(treeList[grid.cells[currentHexIndex].treeIndecies[i]]);
            }
            findNeighbours();
        }
    }

}
