﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class moveGPS : MonoBehaviour {
    public GPSfilter movementType;
    public bool rotateNorth = true;
    float lastRotation = 0;
    Quaternion initialRotation;
    Vector3 tempTransform;
    float currentHeight=0;
    public double Lat, Lon;


	// Use this for initialization
	void Start () {
        initialRotation = transform.rotation;

    }
	
	// Update is called once per frame
	void Update () {
        if (GPSManager.isConnected)
        {
            switch (movementType)
            {
                case GPSfilter.average:
                    transform.position = GPSManager.getAveragePostion;
                    break;
                case GPSfilter.raw:
                    transform.position = GPSManager.getGPSPosition;
                    break;
                    case GPSfilter.stationary:
                    transform.position = GPSManager.getUnityPosition(Lat, Lon);
                    break;
                default:
                    transform.position = GPSManager.getGPSPosition;
                    break;
            }
            if (rotateNorth)
            {
                transform.rotation = Quaternion.Euler(0, GPSManager.northRotation, 0) *initialRotation;
            }
        }
        tempTransform = transform.position;
        tempTransform.y = currentHeight;
        transform.position = tempTransform;


    }
}
