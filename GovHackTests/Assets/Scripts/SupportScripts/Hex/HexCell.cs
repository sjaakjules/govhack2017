﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class HexCell {

    public float canopy2011;
    public float canopy2016;
    public float canopyChange;
    public List<int> treeIndecies = new List<int>();



    public readonly HexCoordinates coordinates;
    public HexDirection Direction;
        
    Vector3 cellPosition;
    Vector3 currentPosition;

    public bool hasUpdated;
    
    HexCell[] neighbors = new HexCell[6];

    int[] neighbourGridIndex;

    static int tempIndex;

    public Vector3 CellPosition { get { return cellPosition; } }

    public HexCell(Vector3 _position, double can11, double can16, double canChange)
    {
        this.coordinates = HexCoordinates.hFp(_position);
        this.cellPosition = _position;
        this.currentPosition = _position;
        Direction = (HexDirection)Random.Range(0, 6);

        canopy2011 = (float)can11;
        canopy2016 = (float)can16;
        canopyChange = (float)canChange;

    }
    

    public HexCell(HexCell cell)
    {
        this.coordinates = cell.coordinates;
        this.cellPosition = cell.cellPosition;
        this.currentPosition = cell.currentPosition;
		this.Direction= cell.Direction;
    }
    
    /// <summary>
    /// Returns the hex cell in the direction specified. Will return null otherwise.
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public HexCell GetNeighbor(HexDirection direction)
    {
        return neighbors[(int)direction];
    }

    /// <summary>
    /// DO NOT USE! 
    /// This is called to populate the CA grid and should not be used afterwards.
    /// Sets this cell with a neighbor in the direction specified. Also sets this cell as the neighbor in the oposite direction.
    /// </summary>
    /// <param name="direction"></param> Direction from this cell to the neighbor cell passed into this function
    /// <param name="cell"></param> The neighbor cell passed into this function which is a neighbor.
    public void SetNeighbor(HexDirection direction, HexCell cell)
    {
        neighbors[(int)direction] = cell; 
        cell.neighbors[(int)direction.Opposite()] = this;
    }

    public void updateGridIndex()
    {
        List<int> tempIndex = new List<int>(6);
        for (int i = 0; i < neighbors.Length; i++)
        {
            if (neighbors[i] != null)
            {
             //   tempIndex.Add(HexCoordinates.iFh(neighbors[i].coordinates));
            }
        }
    }
    

}
