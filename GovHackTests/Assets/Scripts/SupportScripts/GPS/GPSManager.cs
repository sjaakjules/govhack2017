﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum GPSfilter { average, raw, stationary };
public class GPSManager : MonoBehaviour
{

    // ///////////////////////////////////////////////////////////
    //          Static variables accessed externally
    // ///////////////////////////////////////////////////////////    
    public static float latitude, longitude;            // The latest latitude / longitude values

    static Vector3 position, averagePosition;     // The vector3 positions of current GPS, (in m not 1:100)
    static float altitude;                              // Altitude according to GPS but horribly inaccurate.
    static float accuracy;                              // (m) accuracy of X/Z GPS (3m is best, 9m average)
    public static bool isSimulated;
    public static float northRotation;

    static bool gpsFix = false;                    // Bool when GPS is connected

    // Static variables for GPS settings, (must only have one value)
    static float gpsAccuracy = 0.5f;               // Accuracy of the GPS, less than 3 doesnt have much diffrence
    static float gpsUpdate = 0.5f;                 // Accuracy of the GPS, less than 3 doesnt have much diffrence

    public static bool isMoving = false;
    int movementCount = 0;
    int positionupDates = 0;
    public int movementFilterSize = 5;
    static float lastLat, lastLon;

    static AverageFilter aveLat, aveLon, aveHeadCos, aveHeadSin;

    public Text ErrorText;

    // ///////////////////////////////////////////////////////////
    //          variables set in the inspector
    // ///////////////////////////////////////////////////////////
    public bool GeolocateOrigin = true;     // Used to toggle origin from initial GPS or specified
    bool simGPS;                     // used when no GPS is possible, can move with arrows
    public float simSpeed = 1.5f;                  // movement speed when simulated
    public int averageSize = 10;

    // /////////////////////////////////////////////////////////
    //
    //           starting Latitude and Longitude
    //           inspector clips trailing zeros!
    //
    // /////////////////////////////////////////////////////////

    public double startLon = 144.978589f;    // Start location to offset origin
    public double startLat = -37.870552f;    // Start location to offset origin
    // melb uni -37.797136, 144.962265
    /////NOTE: for gameday, change lat long positions back to startLon = 145.002252f and  startLat = -37.795234f!!!!!!!!



    // The latest GPS variables 
    public LocationInfo location;                  // Location data with new information

    Vector3 gpsPosition;                    // GPS location,
    //float Longitude, Latitude;              // GPS longitude and latitude

    static double initLon;                          // The location where the GPS was established. 
    static double initLat;
    public static double initEast, initNorth;

    float GPSupdateRate = 0.5f;             // (seconds) time rate to check for updated GPS values
    float smoothSpeed = 0.1f;               // The percentage jumps per GPSupdateRate to move to new position,

    static string status;                          // String used to report the status of the GPS
    static string setupStatus;

    //Vector3 smoothedPosition;               // Smoothed values, this interpolates towards latest location

    // External gameObjects and scripts used
    //Text errorText;                         // ErrorText text displayed to screen
    //Transform GPSTransform;                 // Will transform the object with the "GPSTransform" tag
    //Transform smoothGPSTransform;           // Will transform the object with the "Player" tag

    //bool foundErrorText = false;            
    //bool foundGPSTransform = false;
    //bool foundSmoothTransform = false;

    // Variables able to be seen externally
    public static Vector3 getGPSPosition { get { if (gpsFix) return position; return Vector3.zero; } }
    public static Vector3 getAveragePostion { get { if (gpsFix) return averagePosition; return Vector3.zero; } }
    public static Vector2 getLatLon { get { if (gpsFix) return new Vector2(latitude, longitude); return Vector2.zero; } }
    public static bool isConnected { get { return gpsFix; } }
    public static float Accuracy { get { if (gpsFix) return accuracy; return float.NaN; } }
    public static string Info { get { return status; } }
    public static float Heading { get { return Mathf.Atan2(aveHeadSin.GetAverage, aveHeadCos.GetAverage) * Mathf.Rad2Deg; } }

    void Awake()
    {
        aveLat = new AverageFilter(averageSize);
        aveLon = new AverageFilter(averageSize);
        aveHeadCos = new AverageFilter(averageSize);
        aveHeadSin = new AverageFilter(averageSize);
    }

    IEnumerator Start()
    {

        InvokeRepeating("UpdateErrorText", 1, 0.5f);

        //STARTING LOCATION SERVICES
        // First, check if user has location service enabled
#if (UNITY_IOS && !UNITY_EDITOR)
		if (!Input.location.isEnabledByUser){
 
			//This message prints to the Editor Console
			print("Please enable location services and restart the App");
			//You can use this "status" variable to show messages in your custom user interface (GUIText, etc.)
			setupStatus = "Please enable location services\n and restart the App";
			yield return new WaitForSeconds(4);
			//Application.Quit();
		}
#endif

#if (UNITY_EDITOR)
        if (!Input.location.isEnabledByUser)
        {
            //This message prints to the Editor Console
            setupStatus = ("Please enable location services... \nRunning app in Simulation Mode");
            simGPS = true;
            yield return new WaitForSeconds(4);
            //Application.Quit();
        }
#endif

        isSimulated = simGPS;
        //Setting variables values on Start
        gpsFix = false;

        // Start service before querying location
        Input.location.Start(gpsAccuracy, gpsUpdate);
        Input.compass.enabled = true;
        setupStatus = "Initializing Location Services..\n";

        // (seconds) Wait time for the GPS to be initialised after which progrom will exit
        int maxWait = 10;
        // Wait until service initializes
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }
        // Service didn't initialize in 30 seconds
        if (maxWait < 1)
        {
            setupStatus = setupStatus + "Unable to initialize location services.\nPlease check your location settings\n and restart the App\n";

            simGPS = true;
            isSimulated = true;
            yield return new WaitForSeconds(2);
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            setupStatus = setupStatus + "Unable to determine your location.\nPlease check your location settings\n and restart this App\n";

            //Application.Quit();
            simGPS = true;
            isSimulated = true;
            yield return new WaitForSeconds(1);
        }
        else
        {
            // Access granted and location value could be retrieved
            setupStatus = setupStatus + "GPS Fix established!\n";

            // (seconds) Time after GPS is connected to establish better connection.
            int initTime = 1;
            //Wait in order to find enough satellites and increase GPS accuracy
            yield return new WaitForSeconds(initTime);

            if (!simGPS && GeolocateOrigin)
            {
                //Set position
                location = Input.location.lastData;
                initLon = location.longitude;
                initLat = location.latitude;
            }
            else
            {
                initLon = startLon;
                initLat = startLat;
            }

            latitude = (float)initLat;
            longitude = (float)initLon;
            convMGA94(initLat, initLon, out initEast, out initNorth);

            averagePosition = Vector3.zero;
            //Successful GPS fix
            gpsFix = true;
        }

        //Set player's position using new location data (every "updateRate" seconds)
        //Default value for updateRate is 0.1. Increase if necessary to improve performance
        InvokeRepeating("MyPosition", 1, isSimulated? 0.05f: GPSupdateRate);


        //Get altitude and horizontal accuracy readings using new location data (Default: every 2s)
        InvokeRepeating("AccuracyAltitude", 3, 2);


    }

    void FixedUpdate()
    {
        if (isSimulated)
        {
            //Use keyboard input to move the player
            if (Input.GetKey("up") || Input.GetKey("w"))
            {
                latitude += (simSpeed * (float)1E-5);
            }
            if (Input.GetKey("down") || Input.GetKey("s"))
            {
                latitude -= (simSpeed * (float)1E-5);
            }
            //Use keyboard input to move the player
            if (Input.GetKey("left") || Input.GetKey("a"))
            {
                longitude -= (simSpeed * (float)1E-5);
            }
            if (Input.GetKey("right") || Input.GetKey("d"))
            {
                longitude += (simSpeed * (float)1E-5);
            }
            if (Input.GetKey("t"))
            {
                northRotation += 1f;
            }
        }
    }

    void MyPosition()
    {
        if (gpsFix)
        {
            lastLat = latitude;
            lastLon = longitude;

            if (!simGPS)
            {
                location = Input.location.lastData;

                gpsPosition = getUnityPosition(location.latitude, location.longitude);

                // Set static long/lat variables
                longitude = location.longitude;
                latitude = location.latitude;
                


                if (!float.IsNaN(aveHeadCos.GetAverage))
                {
                    for (int i = 0; i < averageSize; i++)
                    {
                        aveHeadCos.add(Mathf.Cos(Mathf.Deg2Rad * Input.compass.trueHeading));
                    }
                }
                else
                {
                    aveHeadCos.add(Mathf.Cos(Mathf.Deg2Rad * Input.compass.trueHeading));
                }
                if (!float.IsNaN(aveHeadSin.GetAverage))
                {
                    for (int i = 0; i < averageSize; i++)
                    {
                        aveHeadSin.add(Mathf.Sin(Mathf.Deg2Rad * Input.compass.trueHeading));
                    }
                }
                else
                {
                    aveHeadSin.add(Mathf.Sin(Mathf.Deg2Rad * Input.compass.trueHeading));
                }



            }
            else
            {
                /*
                //Use keyboard input to move the player
                if (Input.GetKey("up") || Input.GetKey("w"))
                {
                    gpsPosition.z += (simSpeed * GPSupdateRate) / 100;
                }
                if (Input.GetKey("down") || Input.GetKey("s"))
                {
                    gpsPosition.z -= (simSpeed * GPSupdateRate) / 100;
                }
                //Use keyboard input to move the player
                if (Input.GetKey("left") || Input.GetKey("a"))
                {
                    gpsPosition.x -= (simSpeed * GPSupdateRate) / 100;
                }
                if (Input.GetKey("right") || Input.GetKey("d"))
                {
                    gpsPosition.x += (simSpeed * GPSupdateRate) / 100;
                }
                if (Input.GetKey("t"))
                {
                    northRotation += 1f;
                }
                */

                gpsPosition = getUnityPosition(latitude, longitude);
                //longitude = (float)initLon + (18000 * (gpsPosition.x)) / 20037508.34f;
                // longitude = (float)initLat + ((360 / Mathf.PI) * Mathf.Atan(Mathf.Exp(0.00001567855943f * (gpsPosition.z)))) - 90;

            }

            if (longitude != lastLon || latitude != lastLat)
            {
                movementCount++;
            }

            if (positionupDates > movementFilterSize)
            {
                if (movementCount > 0)
                {
                    isMoving = true;
                }
                else
                {
                    isMoving = false;
                }
                positionupDates = -1;
                movementCount = 0;
            }
            positionupDates++;


            // Set static postion variables
            position = gpsPosition;
            // Update the average filter
            aveLat.add(latitude);
            aveLon.add(longitude);

            updateAverageValues();
        }
    }



    void AccuracyAltitude()
    {
        if (gpsFix)
        {
            altitude = location.altitude;
            accuracy = location.horizontalAccuracy;
        }
    }

    void UpdateErrorText()
    {
        status = "";
        status = setupStatus + "\n";
        status = status + string.Format("GPS loc:  {0:F7} : {1:F7}\n", latitude, longitude);
        status = status + string.Format("GPS loc:  {0:F5} : {1:F5} : {2:F5}\n", position.x, position.y, position.z);
        status = status + string.Format("User loc: {0:F5} : {1:F5} : {2:F5}\n", averagePosition.x, averagePosition.y, averagePosition.z);
        status = status + string.Format("Heading:  {0}\n", Heading);
        status = status + string.Format("Altitude: {0}\nAccuracy: {1}\n", altitude, accuracy);

        if (ErrorText != null)
        {
            ErrorText.text = status;
        }
    }


    void updateAverageValues()
    {
        if (!float.IsNaN(aveLat.GetAverage) && !float.IsNaN(aveLon.GetAverage))
        {
            // Get new average for positions
            averagePosition = getUnityPosition(aveLat.GetAverage, aveLon.GetAverage);
        }
        


    }


    public static Vector3 getUnityPosition(double Lat, double Lon)
    {
        double east, north;
        convMGA94(Lat, Lon, out east, out north);
        return new Vector3((float)(east - initEast), 0, (float)(north - initNorth));
    }


    static void convMGA94(double Lat, double Long, out double Easting, out double Northing)
    {
        //a = 6,378,137.0 metres; 1/f = 298.25722210
        //Lat=coords(:,1);
        //Long=coords(:,2);
        //// Constants
        double a = 6378137.000;//3
        double finv = 298.257222101000;//4
        double f = 1 / finv;//5and7
        double b = a * (1 - f);//6
        double e2 = 2 * f - System.Math.Pow(f, 2);//8
        double e = System.Math.Sqrt(e2);//9
        double Sece2 = e2 / (1 - e2);//10
        double edash = System.Math.Sqrt(Sece2);//11
        double n = (a - b) / (a + b);//13
        double n2 = System.Math.Pow(n, 2);//14
        double n3 = System.Math.Pow(n, 3);//15
        double n4 = System.Math.Pow(n, 4);//16
        double G = a * (1 - n) * (1 - n2) * (1 + 9 * n2 / 4 + 255 * n4 / 64) * System.Math.PI / 180;//17
        double FalseEast = 500000.0000;//20
        double FalseNorth = 10000000.0000;//21
        double CSF = 0.9996;//22
        double Zonewidth = 6;//23
        double Longcenmer = -177;//24
        double LongWE = Longcenmer - 1.5 * Zonewidth;//25
        double CentMerZZ = LongWE + Zonewidth / 2;//26
                                                  //// Translate DD to radians
        double Latr = Lat * System.Math.PI / 180;
        double Longr = Long * System.Math.PI / 180;
        //// 
        double Zonereal = (Long - LongWE) / Zonewidth;
        double Zone = System.Math.Floor(Zonereal);
        double CentMer = Zonewidth * Zone + CentMerZZ;
        //// Functions
        double Sinlat = System.Math.Sin(Latr);
        double Sin2lat = System.Math.Sin(2 * Latr);
        double Sin4lat = System.Math.Sin(4 * Latr);
        double Sin6lat = System.Math.Sin(6 * Latr);
        // es again
        double e4 = System.Math.Pow(e2, 2);
        double e6 = System.Math.Pow(e2, 3);
        // Radii of curvature
        double rho = a * (1 - e2) / System.Math.Pow(1 - e2 * System.Math.Pow(Sinlat, 2), 1.5);
        double Nu = a / System.Math.Pow(1 - e2 * System.Math.Pow(Sinlat, 2), 0.5);
        //
        double A0 = 1 - (e2 / 4) - ((3 * e4 / 64)) - (5 * e6 / 256);
        double A2 = (3 / 8) * (e2 + e4 / 4 + (15 * e6 / 128));
        double A4 = (15 / 256) * (e4 + 3 * e6 / 4);
        double A6 = 35 * e6 / 3072;
        // Meridian distance
        double M1 = a * A0 * Latr;
        double M2 = -a * A2 * Sin2lat;
        double M3 = a * A4 * Sin4lat;
        double M4 = -a * A6 * Sin6lat;
        double Msum = M1 + M2 + M3 + M4;
        // Cos Lat
        double C1 = System.Math.Cos(Latr);
        double C2 = System.Math.Pow(C1, 2);
        double C3 = System.Math.Pow(C1, 3);
        double C4 = System.Math.Pow(C1, 4);
        double C5 = System.Math.Pow(C1, 5);
        double C6 = System.Math.Pow(C1, 6);
        double C7 = System.Math.Pow(C1, 7);
        double C8 = System.Math.Pow(C1, 8);
        // Diff Long
        double DL1 = (Longr) - (CentMer) * Mathf.PI / 180;
        double DL2 = System.Math.Pow(DL1, 2);
        double DL3 = System.Math.Pow(DL1, 3);
        double DL4 = System.Math.Pow(DL1, 4);
        double DL5 = System.Math.Pow(DL1, 5);
        double DL6 = System.Math.Pow(DL1, 6);
        double DL7 = System.Math.Pow(DL1, 7);
        double DL8 = System.Math.Pow(DL1, 8);
        //Tan Lat
        double TanLat1 = System.Math.Tan(Latr);
        double TanLat2 = System.Math.Pow(TanLat1, 2);
        double TanLat4 = System.Math.Pow(TanLat1, 4);
        double TanLat6 = System.Math.Pow(TanLat1, 6);
        // Psi
        double Psi1 = Nu / rho;
        double Psi2 = System.Math.Pow(Psi1, 2);
        double Psi3 = System.Math.Pow(Psi1, 3);
        double Psi4 = System.Math.Pow(Psi1, 4);
        //// Easting
        double E1 = Nu * DL1 * C1;
        double E2 = Nu * DL3 * C3 * (Psi1 - TanLat2) / 6;
        double E3 = Nu * DL5 * C5 * (4 * Psi3 * (1 - 6 * TanLat2) + Psi3 * (1 + 8 * TanLat2) - Psi1 * 2 * TanLat2 + TanLat4) / 120;
        double E4 = Nu * DL7 * C7 * (61 - 479 * TanLat2 + 179 * TanLat4 - TanLat6) / 5040;
        double Esum = E1 + E2 + E3 + E4;
        double Esumk0 = CSF * Esum;

        Easting = Esumk0 + FalseEast;

        //// Northing
        double N1 = Nu * Sinlat * DL2 * C1 / 2;
        double N2 = Nu * Sinlat * DL4 * C3 * (4 * Psi2 + Psi1 - TanLat2) / 24;
        double N3 = Nu * Sinlat * DL6 * C5 * (8 * Psi4 * (11 - 24 * TanLat2) - 28 * Psi3 * (1 - 6 * TanLat2) + Psi2 * (1 - 32 * TanLat2) - Psi1 * 2 * TanLat2 + TanLat4) / 720;
        double N4 = Nu * Sinlat * DL8 * C7 * (1385 - 3111 * TanLat2 + 543 * TanLat4 - TanLat6) / 40320;
        double Nsum = Msum + N1 + N2 + N3 + N4;
        double Nsumk0 = CSF * Nsum;

        Northing = FalseNorth + Nsumk0;

    }

}
