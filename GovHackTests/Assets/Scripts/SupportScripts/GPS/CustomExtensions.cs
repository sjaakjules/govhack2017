﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public static class CustomExtensions {

    private static System.Random rng = new System.Random();

    public static Quaternion multiply(this Quaternion q1, Quaternion q2)
    {
        Quaternion quaternionOut = Quaternion.identity;
        quaternionOut.w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;
        quaternionOut.x = q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y;
        quaternionOut.y = q1.w * q2.y + q1.y * q2.w + q1.z * q2.x - q1.x * q2.z;
        quaternionOut.z = q1.w * q2.z + q1.z * q2.w + q1.x * q2.y - q1.y * q2.x;
        return quaternionOut;
    }

        public static float normalMap (this float input, float In_L, float In_H, float Out_L, float Out_H)
        {
            float num = 0;
            float range = In_H - In_L;
            float rangeOut = Out_H - Out_L;
            input = (input < In_L ? In_L : input);
            input = (input > In_H ? In_H : input);
            num = range == 0 ? 0 : input / range;
            num = (num * rangeOut) + Out_L;
            return num;
        }
    public static Vector3 divide(this Vector3 vector, float number)
    {
        return new Vector3(1.0f * vector.x / number, 1.0f * vector.y / number, 1.0f * vector.z / number);
    }

    public static Vector3 multiply(this Vector3 vector, float number)
    {
        return new Vector3(vector.x * number, vector.y * number, vector.z * number);
    }

    public static Vector3 multiply(this Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
    }

    public static Vector3 add(this Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
    }
    public static Vector3 add(this Vector3 v1, float  v2)
    {
        return new Vector3(v1.x + v2, v1.y + v2, v1.z + v2);
    }


    public static void Shuffle<T>(this List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static int findMaxIndicie(this int[] intArray)
    {
        int maxAmount = 0;
        int highestIndicie = 0;

        for (int i = 0; i < intArray.Length; i++)
        {
            if (intArray[i] > maxAmount)
            {
                maxAmount = intArray[i];
                highestIndicie = i;
            }
        }
        return highestIndicie;

    }

    public static int findMinIndicie(this int[] intArray)
    {
        int minAmount = 1;
        int lowestIndicie = 0;

        for (int i = 0; i < intArray.Length; i++)
        {
            if (intArray[i] != 0 && intArray[i] <= minAmount)
            {
                minAmount = intArray[i];
                lowestIndicie = i;
            }
        }
        return lowestIndicie;

    }

    public static int findMaxValue(this int[] intArray)
    {
        int maxAmount = 0;

        for (int i = 0; i < intArray.Length; i++)
        {
            if (intArray[i] >= maxAmount)
            {
                maxAmount = intArray[i];
            }
        }
        return maxAmount;
    }


}


public class AverageFilter
{
    float[] values;
    int size;
    int currentPosition;
    bool hasLooped = false;
    float average = float.NaN;

    public float GetAverage { get { return average; } }

    public AverageFilter(int _size)
    {
        size = _size;
        values = new float[_size];
        currentPosition = 0;
    }

    public bool add(float number)
    {
        if (float.IsNaN(number) || float.IsInfinity(number))
        {
            return false;
        }
        else
        {
            values[currentPosition] = number;
            currentPosition++;
            if (currentPosition >= size)
            {
                hasLooped = true;
                currentPosition = 0;
            }
            average = getAverage();
        }
        return true;
    }

    float getAverage()
    {
        if (currentPosition > 0)
        {
            if (hasLooped)
            {
                return values.Average();
            }
            else
            {
                float sum = 0;
                for (int i = 0; i < currentPosition - 1; i++)
                {
                    sum += values[i];
                }
                return sum / (currentPosition - 1);
            }
        }
        return float.NaN;
    }
}
