﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjNet;

public class CSVManager : MonoBehaviour {

    public TextAsset[] CSVFiles;
    CSVreader myCSVs;
    public static bool hasDigested = false;

    
    public string[] GridCSVHeadings;
    public string[] TreeCSVHeadings;
    public string[] TestDataCSVHeadings;
    public string[] testData;

    public GameObject gridObject;

    static public Vector3[] GridLocation, TreeLocation, testPoints;
    static public float[,] TreeAge;

    public int x;

    void OnValidate()
    {
        myCSVs = new CSVreader(CSVFiles);
        GridCSVHeadings = myCSVs.ColumnNames[0].ToArray();
        TreeCSVHeadings = myCSVs.ColumnNames[1].ToArray();
        TestDataCSVHeadings = myCSVs.ColumnNames[3].ToArray();
        testData = myCSVs.CSVText[1][4];
    }



    // Use this for initialization
    IEnumerator Start () {
        myCSVs = new CSVreader(CSVFiles);
        




        GridLocation = new Vector3[myCSVs.CSVData[0].Count]; //GridLocation is now an array of Vector3's of myCSVs.CSVData[0].Count length
        TreeLocation = new Vector3[myCSVs.CSVData[1].Count];
        testPoints = new Vector3[myCSVs.CSVData[3].Count];

        GridCSVHeadings = myCSVs.ColumnNames[0].ToArray();
        TreeCSVHeadings = myCSVs.ColumnNames[1].ToArray();
        TestDataCSVHeadings = myCSVs.ColumnNames[3].ToArray();


        TreeAge = new float[myCSVs.CSVData[1].Count, 2];

        while (!GPSManager.isConnected)
        {
            yield return new WaitForSeconds(1);
        }
        digestCSVFiles();
    }


    void digestCSVFiles()
    {

        Data.grid = new HexGrid(myCSVs.CSVData[0], 25 / HexMetrics.RadiusChange);
        Data.treeList = new TreeInfo[myCSVs.CSVText[1].Count];
        // Data.treeList = new TreeInfo(myCSVs.CSVText[1], myCSVs.CSVData[1]);
        for (int i = 0; i < myCSVs.CSVText[1].Count; i++)
        {
            Data.treeList[i] = new TreeInfo(myCSVs.CSVText[1][i]);
            Data.grid.cells[(int)myCSVs.CSVData[1][i][23]].treeIndecies.Add(i);
        }



        for (int i = 0; i < myCSVs.CSVData[0].Count; i++)
        {
            GridLocation[i] = GPSManager.getUnityPosition(myCSVs.CSVData[0][i][1], myCSVs.CSVData[0][i][0]);

        }
        for (int i = 0; i < myCSVs.CSVData[1].Count; i++)
        {
            TreeLocation[i] = GPSManager.getUnityPosition(myCSVs.CSVData[1][i][1], myCSVs.CSVData[1][i][0]);

            TreeAge[i, 0] = (float)myCSVs.CSVData[1][i][9];
            TreeAge[i, 1] = (float)myCSVs.CSVData[1][i][13];
        }
        for (int i = 0; i < myCSVs.CSVData[3].Count; i++)
        {
            // GPSManager.convMGA94(myCSVs.CSVData[3][i][1], myCSVs.CSVData[3][i][0], out East, out north);
            // publicToilets[i] = new Vector3((float)(East - originEast), 0, (float)(north - originNorth));
            testPoints[i] = GPSManager.getUnityPosition(myCSVs.CSVData[3][i][1], myCSVs.CSVData[3][i][0]);
        }

        gridObject.transform.position = GridLocation[0];

        hasDigested = true;
    }
	
	// Update is called once per frame
	void Update () {
        gridObject.transform.position = TreeLocation[x];

    }
}
