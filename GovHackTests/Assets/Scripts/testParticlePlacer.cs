﻿using UnityEngine;
using System.Collections.Generic;

public enum PointsToDraw { Data, DataAndPlayer, Player}

public class testParticlePlacer : MonoBehaviour
{
    public GameObject test;

    public ParticleSystem ParticleSystem;

    public PointsToDraw pointsToDraw;
    

    List<float> TreeAge = new List<float>();

    [Range(0, 255)]
    public byte r, g, b;

    public float size;

    Color partColor;

    /*
         USAGE NOTES
          1. In the particle system disable EVERYTHING except for Renderer.
          
         */

    // Use this for initialization
    void Start()
    {
        double east, north;
      // GPSManager.convMGA94(-37.814955, 144.964493, out east, out north);
       // test.transform.position = new Vector3((float)(east-CSVManager.originEast), 0, (float)(north-CSVManager.originNorth));
        test.transform.position = GPSManager.getUnityPosition(-37.814955, 144.964493);


        
        //TreeAge = new List<float>(CSVManager.TreeAge);



    }
    


    private void LateUpdate()
    {
        if (CSVManager.hasDigested)
        {
            List<Color32> colours = new List<Color32>();
            List<float> sizes = new List<float>();
            List<Vector3[]> positioins = new List<Vector3[]>();
            
            switch (pointsToDraw)
            {
                case PointsToDraw.Data:
                    positioins.Add(Data.grid.cellCentres);
                    colours.Add(new Color32(r, g, b, 255));
                    sizes.Add(size);

                    positioins.Add(Data.treeList.getAllPositions());
                    colours.Add(new Color32(50, 255, 50, 255));
                    sizes.Add(size);

                    DrawMultipleListsParticles(positioins, ParticleSystem, colours, sizes);
                    break;
                case PointsToDraw.DataAndPlayer:
                    positioins.Add(Data.grid.cellCentres);
                    colours.Add(new Color32(r, g, b, 255));
                    sizes.Add(size);

                    positioins.Add(Data.treeList.getAllPositions());
                    colours.Add(new Color32(50, 255, 50, 255));
                    sizes.Add(size/2);

                    positioins.Add(Data.grid.getPositions(Data.surroundingHexIndex.ToArray()));
                    colours.Add(new Color32(0, 255, 0, 255));
                    sizes.Add(size * 2);

                    positioins.Add(new Vector3[] { Data.grid.cellCentres[Data.currentHexIndex] });
                    colours.Add(new Color32(255, 0, 0, 255));
                    sizes.Add(size * 3);

                    positioins.Add(Data.surroundingTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(255, 0, 255, 255));
                    sizes.Add(size * 2);

                    DrawMultipleListsParticles(positioins, ParticleSystem, colours, sizes);
                    break;
                case PointsToDraw.Player:

                    positioins.Add(Data.treeList.getAllPositions());
                    colours.Add(new Color32(50, 255, 50, 255));
                    sizes.Add(size/2);

                    positioins.Add(Data.grid.getPositions(Data.surroundingHexIndex.ToArray()));
                    colours.Add(new Color32(0, 255, 0, 255));
                    sizes.Add(size * 2);

                    positioins.Add(new Vector3[] { Data.grid.cellCentres[Data.currentHexIndex] });
                    colours.Add(new Color32(255, 0, 0, 255));
                    sizes.Add(size * 3);

                    positioins.Add(Data.surroundingTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(50, 255, 50, 255));
                    sizes.Add(size );

                    positioins.Add(Data.currentTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(25, 255, 25, 255));
                    sizes.Add(size * 2);

                    DrawMultipleListsParticles(positioins, ParticleSystem, colours, sizes);
                    break;
                default:
                    break;
            }
        }
         // DrawParticles(GridObjects, ParticleSystem, new Color32(255, 255, 255, 255),10f);
       // DrawParticles(TreeObjects, ParticleSystem, new Color32(r, g, b, 255), size);
      ///  DrawParticles(TreeObjectsEN, ParticleSystem, new Color32(r, g, b, 255), size);
    }

    void DrawMultipleListsParticles(List<Vector3[]> positions, ParticleSystem particleSys, List<Color32> colour, List<float> size)
    {
        ParticleSystem.Particle[] arrParticles;


        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed
        int nParticles = 0;
        for (int i = 0; i < positions.Count; i++)
        {
            nParticles += positions[i].Length;
        }

        arrParticles = new ParticleSystem.Particle[nParticles];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);
        int startIndex = 0;
        // Run a loop through the number of particles.. i is our counter value
        for (int i = 0; i < positions.Count; i++)
        {
            for (int j = 0; j < positions[i].Length; j++)
            {
                // For ease of use assign a ParticleSystem.particle for the current object
                ParticleSystem.Particle par = arrParticles[startIndex + j];

                par.startSize = size[i];
                par.startColor = colour[i];

                //  par.startColor = colour;
                par.position = positions[i][j];
                par.velocity = Vector3.zero;
                par.remainingLifetime = 900f;


                // Critical step! You MUST assign the modified particle BACK into the current position of the particles
                //  This seems to be how the data is saved to the particle.
                arrParticles[startIndex + j] = par;
            }
            startIndex += positions[i].Length;
        }

        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);
    }

    void DrawOneListParticles(Vector3[] positions, ParticleSystem particleSys, Color32 colour, float size)
    {
        ParticleSystem.Particle[] arrParticles;


        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed

        arrParticles = new ParticleSystem.Particle[positions.Length];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);

        // Run a loop through the number of particles.. i is our counter value
        for (int i = 0; i < arrParticles.Length; i++)
        {

            // For ease of use assign a ParticleSystem.particle for the current object
            ParticleSystem.Particle par = arrParticles[i];

            /*
            float age = 2017 - CSVManager.TreeAge[i, 0];
            float lifeEx = CSVManager.TreeAge[i, 1];

            colour = Color.HSVToRGB(1, 1, 1);

            if (age > 0)
            {
                par.startSize = 10;
                par.startColor = colour;
            }
            else
            {
                par.startSize = size;
                par.startColor = new Color32(50, 30, 80, 255);
            }
            */
            if (i == Data.currentHexIndex)
            {
                par.startSize = size * 3;
                par.startColor = new Color32(255, 0, 0, 255);
            }
            else if (Data.surroundingHexIndex.Contains(i))
            {

                par.startSize = size * 2;
                par.startColor = new Color32(0, 255, 0, 255);
            }
            else
            {
                par.startSize = size;
                par.startColor = colour;
            }

            //  par.startColor = colour;
            par.position = positions[i];
            par.velocity = Vector3.zero;
            par.remainingLifetime = 900f;


            // Critical step! You MUST assign the modified particle BACK into the current position of the particles
            //  This seems to be how the data is saved to the particle.
            arrParticles[i] = par;
        }

        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);
    }

    /*
	void GenerateObjects() {

        //listObjects should be populated by whatever means you need - DB, array, text file et
        //listObjects = CAPos;
        listObjects = CSV.dataMap;
        int i = 0;

		theParticleSystem.Clear ();
		theParticleSystem.maxParticles = listObjects.Count;

		foreach (Vector3 obj in listObjects) 
		{
			// Get the X Y and Z coordinates for the object
			float x = obj.x * fDistanceModifier;
            float y = obj.y * fDistanceModifier;
			float z = obj.z * fDistanceModifier;

			// Set the position to a vector3 
			Vector3 objPos = new Vector3 (x,y,z);

			if(showDebugMessages)
				Debug.Log("Position of obj: " + objPos.ToString());

			// Create the game objects that sit at the positions in space where objs exist
			//  Set the position and parent the object accordingly
			//clone.name = obj.name;
			Transform clone = (Transform)Instantiate(objPrefab, objPos, Quaternion.identity);

			clone.SetParent(gameObject.transform);

			i++;
		}

		if(showDebugMessages)
			Debug.Log("Generate GenerateObjects has finished.");
	}

    void whispMovement()
	{
        float offset;
        int i = 0;

        foreach (Vector3 cell in LerpPos)
		{
			offset = Random.Range(-100,100);
			if (listObjects[i].y < CSV.dataMap[i].y)
			{
			LerpPos[i] = new Vector3(cell.x + (offset / 100), cell.y + Mathf.Abs(offset / 100), cell.z + (offset / 100));	
			}
			else
			{
LerpPos[i] = new Vector3(cell.x + (offset / 100), cell.y + (offset / 100), cell.z + (offset / 100));
			}
			
            i++;
        }  
    }
    */
}


public static class ExtensionMethods {
 
public static float Remap (this float value, float from1, float to1, float from2, float to2) {
    return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
}
   
}